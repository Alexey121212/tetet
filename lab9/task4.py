def input_file(path):
    array = []
    with open.file(path, 'r') as file:
        for line in file:
            if line.replace(';', '').split() != []:
                XY = map(int, line.replace(';', '').split())
                array.append(list(XY))
    array[4][0] += 1
    return array

    file = 'data.csv'
    coords = input_file(file)
    for i in coords:
        print(f'x={i[0]:<5} y={i[1]}')
