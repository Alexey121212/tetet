from Task_2 import generate_coords 

def file_output(array, path):
	'''Ввод данных в файл path'''
	with open(path, 'w') as file:
		for i in array:
			file.write(str(i[0]) + '; ' + str(i[1]) + ';\n')
	print('Запись в файл выполнена успешно')

def main(args):
    file = 'data.csv'
    Coords = generate_coords(10)
    file_output(Coords, file)

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
