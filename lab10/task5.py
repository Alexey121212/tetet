import matplotlib.pyplot as plt
from Task4 import input_file

def draw_line(array):
    x=[]
    y=[]
    l=len(array)
    for i in range(l+1):
        x.append(array[i%l][0])
        y.append(array[i%l][1])
    return x,y

def main(args):
    file='data__1_.csv'
    coords=input_file(file)
    x,y=draw_line(coords)
    plt.plot(x,y,'m')
    plt.show()

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
